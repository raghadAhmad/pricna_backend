<!-- Terms Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('terms', 'Terms:') !!}
    {!! Form::textarea('terms', null, ['class' => 'form-control']) !!}
</div>

<!-- Privacy Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('privacy', 'Privacy:') !!}
    {!! Form::textarea('privacy', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Social Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('social', 'Social:') !!}
    {!! Form::textarea('social', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('settings.index') }}" class="btn btn-default">Cancel</a>
</div>
