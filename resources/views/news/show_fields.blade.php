<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $news->title }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $news->description }}</p>
</div>

<!-- Lang Id Field -->
<div class="form-group">
    {!! Form::label('lang_id', 'Lang Id:') !!}
    <p>{{ $news->lang_id }}</p>
</div>

