<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('languages*') ? 'active' : '' }}">
    <a href="{{ route('languages.index') }}"><i class="fa fa-edit"></i><span>Languages</span></a>
</li>

<li class="{{ Request::is('countries*') ? 'active' : '' }}">
    <a href="{{ route('countries.index') }}"><i class="fa fa-edit"></i><span>Countries</span></a>
</li>

<li class="{{ Request::is('cities*') ? 'active' : '' }}">
    <a href="{{ route('cities.index') }}"><i class="fa fa-edit"></i><span>Cities</span></a>
</li>

<li class="{{ Request::is('stores*') ? 'active' : '' }}">
    <a href="{{ route('stores.index') }}"><i class="fa fa-edit"></i><span>Stores</span></a>
</li>

<li class="{{ Request::is('storeLangs*') ? 'active' : '' }}">
    <a href="{{ route('storeLangs.index') }}"><i class="fa fa-edit"></i><span>Store Langs</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{{ route('categories.index') }}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>

<li class="{{ Request::is('categoryLanguages*') ? 'active' : '' }}">
    <a href="{{ route('categoryLanguages.index') }}"><i class="fa fa-edit"></i><span>Category Languages</span></a>
</li>

<li class="{{ Request::is('products*') ? 'active' : '' }}">
    <a href="{{ route('products.index') }}"><i class="fa fa-edit"></i><span>Products</span></a>
</li>

<li class="{{ Request::is('productLangs*') ? 'active' : '' }}">
    <a href="{{ route('productLangs.index') }}"><i class="fa fa-edit"></i><span>Product Langs</span></a>
</li>

<li class="{{ Request::is('productStores*') ? 'active' : '' }}">
    <a href="{{ route('productStores.index') }}"><i class="fa fa-edit"></i><span>Product Stores</span></a>
</li>

<li class="{{ Request::is('productGalleries*') ? 'active' : '' }}">
    <a href="{{ route('productGalleries.index') }}"><i class="fa fa-edit"></i><span>Product Galleries</span></a>
</li>

<li class="{{ Request::is('favourites*') ? 'active' : '' }}">
    <a href="{{ route('favourites.index') }}"><i class="fa fa-edit"></i><span>Favourites</span></a>
</li>

<li class="{{ Request::is('questions*') ? 'active' : '' }}">
    <a href="{{ route('questions.index') }}"><i class="fa fa-edit"></i><span>Questions</span></a>
</li>

<li class="{{ Request::is('answers*') ? 'active' : '' }}">
    <a href="{{ route('answers.index') }}"><i class="fa fa-edit"></i><span>Answers</span></a>
</li>

<li class="{{ Request::is('offers*') ? 'active' : '' }}">
    <a href="{{ route('offers.index') }}"><i class="fa fa-edit"></i><span>Offers</span></a>
</li>

<li class="{{ Request::is('news*') ? 'active' : '' }}">
    <a href="{{ route('news.index') }}"><i class="fa fa-edit"></i><span>News</span></a>
</li>

<li class="{{ Request::is('cobons*') ? 'active' : '' }}">
    <a href="{{ route('cobons.index') }}"><i class="fa fa-edit"></i><span>Cobons</span></a>
</li>

<li class="{{ Request::is('sliders*') ? 'active' : '' }}">
    <a href="{{ route('sliders.index') }}"><i class="fa fa-edit"></i><span>Sliders</span></a>
</li>

<li class="{{ Request::is('sliderLangs*') ? 'active' : '' }}">
    <a href="{{ route('sliderLangs.index') }}"><i class="fa fa-edit"></i><span>Slider Langs</span></a>
</li>

<li class="{{ Request::is('settings*') ? 'active' : '' }}">
    <a href="{{ route('settings.index') }}"><i class="fa fa-edit"></i><span>Settings</span></a>
</li>

