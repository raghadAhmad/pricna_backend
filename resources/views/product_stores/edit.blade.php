@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Product Store
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($productStore, ['route' => ['productStores.update', $productStore->id], 'method' => 'patch']) !!}

                        @include('product_stores.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection