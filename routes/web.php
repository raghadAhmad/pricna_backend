<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');

Route::resource('users', 'UserController');

Route::resource('languages', 'LanguageController');

Route::resource('countries', 'CountryController');

Route::resource('cities', 'CityController');

Route::resource('stores', 'StoreController');

Route::resource('storeLangs', 'StoreLangController');

Route::resource('categories', 'CategoryController');

Route::resource('categoryLanguages', 'CategoryLanguageController');

Route::resource('products', 'ProductController');

Route::resource('productLangs', 'ProductLangController');

Route::resource('productStores', 'ProductStoreController');

Route::resource('productGalleries', 'ProductGalleryController');

Route::resource('favourites', 'FavouriteController');

Route::resource('questions', 'QuestionController');

Route::resource('answers', 'AnswerController');

Route::resource('offers', 'OfferController');

Route::resource('news', 'NewsController');

Route::resource('cobons', 'CobonsController');

Route::resource('sliders', 'SliderController');

Route::resource('sliderLangs', 'SliderLangController');

Route::resource('settings', 'SettingController');