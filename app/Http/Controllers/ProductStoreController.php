<?php

namespace App\Http\Controllers;

use App\DataTables\ProductStoreDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProductStoreRequest;
use App\Http\Requests\UpdateProductStoreRequest;
use App\Repositories\ProductStoreRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ProductStoreController extends AppBaseController
{
    /** @var  ProductStoreRepository */
    private $productStoreRepository;

    public function __construct(ProductStoreRepository $productStoreRepo)
    {
        $this->productStoreRepository = $productStoreRepo;
    }

    /**
     * Display a listing of the ProductStore.
     *
     * @param ProductStoreDataTable $productStoreDataTable
     * @return Response
     */
    public function index(ProductStoreDataTable $productStoreDataTable)
    {
        return $productStoreDataTable->render('product_stores.index');
    }

    /**
     * Show the form for creating a new ProductStore.
     *
     * @return Response
     */
    public function create()
    {
        return view('product_stores.create');
    }

    /**
     * Store a newly created ProductStore in storage.
     *
     * @param CreateProductStoreRequest $request
     *
     * @return Response
     */
    public function store(CreateProductStoreRequest $request)
    {
        $input = $request->all();

        $productStore = $this->productStoreRepository->create($input);

        Flash::success('Product Store saved successfully.');

        return redirect(route('productStores.index'));
    }

    /**
     * Display the specified ProductStore.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productStore = $this->productStoreRepository->find($id);

        if (empty($productStore)) {
            Flash::error('Product Store not found');

            return redirect(route('productStores.index'));
        }

        return view('product_stores.show')->with('productStore', $productStore);
    }

    /**
     * Show the form for editing the specified ProductStore.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productStore = $this->productStoreRepository->find($id);

        if (empty($productStore)) {
            Flash::error('Product Store not found');

            return redirect(route('productStores.index'));
        }

        return view('product_stores.edit')->with('productStore', $productStore);
    }

    /**
     * Update the specified ProductStore in storage.
     *
     * @param  int              $id
     * @param UpdateProductStoreRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductStoreRequest $request)
    {
        $productStore = $this->productStoreRepository->find($id);

        if (empty($productStore)) {
            Flash::error('Product Store not found');

            return redirect(route('productStores.index'));
        }

        $productStore = $this->productStoreRepository->update($request->all(), $id);

        Flash::success('Product Store updated successfully.');

        return redirect(route('productStores.index'));
    }

    /**
     * Remove the specified ProductStore from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productStore = $this->productStoreRepository->find($id);

        if (empty($productStore)) {
            Flash::error('Product Store not found');

            return redirect(route('productStores.index'));
        }

        $this->productStoreRepository->delete($id);

        Flash::success('Product Store deleted successfully.');

        return redirect(route('productStores.index'));
    }
}
