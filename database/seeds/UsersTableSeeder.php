<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\Models\user::create(
            [
                'name'=>'admin',
                'email'=>'admin@admin.com',
                'password'=>encrypt('123123'),
                'type'=>'admin',
            ]
        );

        factory(App\Models\User::class,20)->create();
    }
}
