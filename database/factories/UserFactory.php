<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {

    return [
        'name' => $faker->firstName,
        'email' => $faker->email,
        'email_verified_at' => $faker->date('Y-m-d H:i:s'),
        'password' => $faker->password,
        'img' => $faker->image('public/uploads/images/Users',150,150,null,false),
        'type' => $faker->randomElement(['admin','store','user']),
        'remember_token' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
